
# Music player

This is a simple music player desktop application with the ability to edit basic mp3 tag information and export all mp3 tag information of loaded songs in JSON format.


## Features

- Load albums
- Play/pause, skip song
- Volume control
- Edit mp3 tag of a selected song
- Export mp3 tag in JSON for the whole album

  
## Used libraries

- TagLibSharp
- NAudio
  