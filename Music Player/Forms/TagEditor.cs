﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Music_Player.InputOutput;
using Music_Player.Models;
using TagLib;

namespace Music_Player.Forms
{
    public partial class TagEditor : Form
    {
        /// <summary>
        ///     Song with the tag to which the changes are being made
        /// </summary>
        private readonly Song _song;

        /// <summary>
        ///     Temporary path to picture for adding it later to the tag
        /// </summary>
        private string _tempPicturePath;

        public TagEditor(Song song)
        {
            InitializeComponent();
            _song = song;
            Text = $@"Tag Editor - {_song.Name}";
            LoadTextBoxes(false);
        }

        /// <summary>
        ///     Parses values from textBoxes into the tag of the song and applies changes
        ///     SAVES the tag!
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonChangesApply_Click(object sender, EventArgs e)
        {
            var performersList = new List<string>(textBoxArtists.Text.Split(","))
                .Where(artist => !string.IsNullOrWhiteSpace(artist)).Select(artist => artist.Trim()).ToArray();
            var genresList = new List<string>(textBoxGenres.Text.Split(","))
                .Where(genre => !string.IsNullOrWhiteSpace(genre)).Select(genre => genre.Trim()).ToArray();
            var albumArtistsList = new List<string>(textBoxAlbumArtists.Text.Split(","))
                .Where(artist => !string.IsNullOrWhiteSpace(artist)).Select(artist => artist.Trim()).ToArray();
            var composersList = new List<string>(textBoxComposers.Text.Split(","))
                .Where(composer => !string.IsNullOrWhiteSpace(composer)).Select(composer => composer.Trim()).ToArray();

            var parseYear = uint.TryParse(textBoxYear.Text, out var year);
            var parseTrack = uint.TryParse(textBoxTrack.Text, out var track);
            var parseDisc = uint.TryParse(textBoxDiscNumber.Text, out var discNumber);

            _song.Tag.Title = string.IsNullOrWhiteSpace(textBoxTitle.Text) ? "" : textBoxTitle.Text.Trim();
            _song.Tag.Performers = performersList;
            _song.Tag.Album = string.IsNullOrWhiteSpace(textBoxAlbum.Text) ? "" : textBoxAlbum.Text.Trim();
            if (parseYear) _song.Tag.Year = year;
            if (parseTrack) _song.Tag.Track = track;
            _song.Tag.Genres = genresList;
            _song.Tag.Comment = string.IsNullOrWhiteSpace(textBoxComment.Text) ? "" : textBoxComment.Text.Trim();
            _song.Tag.AlbumArtists = albumArtistsList;
            _song.Tag.Composers = composersList;
            if (parseDisc) _song.Tag.Disc = discNumber;

            if (_tempPicturePath != null && !string.IsNullOrWhiteSpace(_tempPicturePath))
            {
                var tempList =
                    new List<IPicture>(_song.Tag.Pictures.Where(picture => picture.Type != PictureType.FrontCover));

                tempList.Insert(0, Loader.LoadPicture(_tempPicturePath));
                _song.Tag.Pictures = tempList.ToArray();
            }

            _song.TagFile.Save();

            if (!parseYear || !parseTrack || !parseDisc)
            {
                labelError.ForeColor = Color.Red;
                labelError.Text = "The entered value is not a valid number!";
            }
            else
            {
                labelError.ForeColor = Color.Green;
                labelError.Text = "Changes applied.";
            }
        }

        /// <summary>
        ///     Resets the tag of the song to the default("factory") values
        ///     SAVES the tag!
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonChangesReset_Click(object sender, EventArgs e)
        {
            _song.Tag.Title = _song.FactoryTagProperties.Title;
            _song.Tag.Performers = _song.FactoryTagProperties.Performers;
            _song.Tag.Album = _song.FactoryTagProperties.Album;
            _song.Tag.Year = _song.FactoryTagProperties.Year;
            _song.Tag.Track = _song.FactoryTagProperties.Track;
            _song.Tag.Genres = _song.FactoryTagProperties.Genres;
            _song.Tag.Comment = _song.FactoryTagProperties.Comment;
            _song.Tag.AlbumArtists = _song.FactoryTagProperties.AlbumArtists;
            _song.Tag.Composers = _song.FactoryTagProperties.Composers;
            _song.Tag.Disc = _song.FactoryTagProperties.Disc;
            _song.Tag.Pictures = _song.FactoryTagProperties.Covers;

            LoadTextBoxes(true);

            _song.TagFile.Save();

            labelError.ForeColor = Color.Green;
            labelError.Text = "Reset to default values.";
        }

        /// <summary>
        ///     Adds picture path to temporary path attribute, so it can by applied later to the tag
        ///     Opens file dialog window to select desired file to be put as song cover image tag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddCover_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK) _tempPicturePath = openFileDialog1.FileName;
            labelCoverStatus.Text = string.IsNullOrWhiteSpace(_tempPicturePath) ? "Invalid path..." : _tempPicturePath;
        }

        /// <summary>
        ///     Loads default("factory") Tag values into textBoxes
        /// </summary>
        private void LoadTextBoxes(bool defaultValues)
        {
            if (defaultValues)
            {
                textBoxTitle.Text = _song.FactoryTagProperties.Title;
                textBoxArtists.Text = string.Join(", ", _song.FactoryTagProperties.Performers);
                textBoxAlbum.Text = _song.FactoryTagProperties.Album;
                textBoxYear.Text = _song.FactoryTagProperties.Year.ToString();
                textBoxTrack.Text = _song.FactoryTagProperties.Track.ToString();
                textBoxGenres.Text = string.Join(", ", _song.FactoryTagProperties.Genres);
                textBoxComment.Text = _song.FactoryTagProperties.Comment;
                textBoxAlbumArtists.Text = string.Join(", ", _song.FactoryTagProperties.AlbumArtists);
                textBoxComposers.Text = string.Join(", ", _song.FactoryTagProperties.Composers);
                textBoxDiscNumber.Text = _song.FactoryTagProperties.Disc.ToString(); 
                return;
            }
            
            textBoxTitle.Text = _song.Tag.Title;
            textBoxArtists.Text = string.Join(", ", _song.Tag.Performers);
            textBoxAlbum.Text = _song.Tag.Album;
            textBoxYear.Text = _song.Tag.Year.ToString();
            textBoxTrack.Text = _song.Tag.Track.ToString();
            textBoxGenres.Text = string.Join(", ", _song.Tag.Genres);
            textBoxComment.Text = _song.Tag.Comment;
            textBoxAlbumArtists.Text = string.Join(", ", _song.Tag.AlbumArtists);
            textBoxComposers.Text = string.Join(", ", _song.Tag.Composers);
            textBoxDiscNumber.Text = _song.Tag.Disc.ToString(); 
            
        }

        /// <summary>
        ///     Removes pictures from song tag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRemoveCover_Click(object sender, EventArgs e)
        {
            _song.Tag.Pictures = Array.Empty<IPicture>();
            labelCoverStatus.Text = "Removed...";
        }

        /// <summary>
        ///     Calls buttonChangesApply_Click and closes tag editor form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {
            buttonChangesApply_Click(sender, e);
            Close();
        }
    }
}