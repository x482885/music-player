﻿using System.Drawing;

namespace Music_Player.Forms
{
    partial class Primary
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Primary));
            this.listLoadedSongs = new System.Windows.Forms.ListBox();
            this.buttonLoadDirectory = new System.Windows.Forms.Button();
            this.buttonEditTag = new System.Windows.Forms.Button();
            this.sliderVolume = new System.Windows.Forms.TrackBar();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPrevious = new System.Windows.Forms.Button();
            this.buttonPlayPause = new System.Windows.Forms.Button();
            this.tableLayoutSongTags = new System.Windows.Forms.TableLayoutPanel();
            this.labelSongArtist = new System.Windows.Forms.Label();
            this.labelSongAlbum = new System.Windows.Forms.Label();
            this.labelSongTitle = new System.Windows.Forms.Label();
            this.labelSongYear = new System.Windows.Forms.Label();
            this.labelSongGenre = new System.Windows.Forms.Label();
            this.labelSongComposer = new System.Windows.Forms.Label();
            this.labelSongArtistValue = new System.Windows.Forms.Label();
            this.labelSongAlbumValue = new System.Windows.Forms.Label();
            this.labelSongYearValue = new System.Windows.Forms.Label();
            this.labelSongGenreValue = new System.Windows.Forms.Label();
            this.labelSongComposerValue = new System.Windows.Forms.Label();
            this.labelSongTitleValue = new System.Windows.Forms.Label();
            this.pictureSong = new System.Windows.Forms.PictureBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.labelErrorMessage = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelSongCodec = new System.Windows.Forms.Label();
            this.labelSongBitrate = new System.Windows.Forms.Label();
            this.labelSongFrequency = new System.Windows.Forms.Label();
            this.labelSongLength = new System.Windows.Forms.Label();
            this.labelSongCodecValue = new System.Windows.Forms.Label();
            this.labelSongBitrateValue = new System.Windows.Forms.Label();
            this.labelSongFrequencyValue = new System.Windows.Forms.Label();
            this.labelSongLengthValue = new System.Windows.Forms.Label();
            this.buttonExport = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.folderBrowserDialog2 = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize) (this.sliderVolume)).BeginInit();
            this.tableLayoutSongTags.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.pictureSong)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listLoadedSongs
            // 
            this.listLoadedSongs.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.listLoadedSongs.FormattingEnabled = true;
            this.listLoadedSongs.Location = new System.Drawing.Point(583, 43);
            this.listLoadedSongs.Name = "listLoadedSongs";
            this.listLoadedSongs.Size = new System.Drawing.Size(205, 342);
            this.listLoadedSongs.TabIndex = 0;
            this.listLoadedSongs.HorizontalScrollbar = true;
            this.listLoadedSongs.DoubleClick += new System.EventHandler(this.listLoadedSongs_DoubleClick);
            // 
            // buttonLoadDirectory
            // 
            this.buttonLoadDirectory.Location = new System.Drawing.Point(583, 10);
            this.buttonLoadDirectory.Name = "buttonLoadDirectory";
            this.buttonLoadDirectory.Size = new System.Drawing.Size(71, 25);
            this.buttonLoadDirectory.TabIndex = 5;
            this.buttonLoadDirectory.Text = "Load";
            this.buttonLoadDirectory.UseVisualStyleBackColor = true;
            this.buttonLoadDirectory.Click += new System.EventHandler(this.buttonLoadDirectoryAsync_Click);
            // 
            // buttonEditTag
            // 
            this.buttonEditTag.Location = new System.Drawing.Point(717, 10);
            this.buttonEditTag.Name = "buttonEditTag";
            this.buttonEditTag.Size = new System.Drawing.Size(71, 25);
            this.buttonEditTag.TabIndex = 6;
            this.buttonEditTag.Enabled = false;
            this.buttonEditTag.Text = "Edit";
            this.buttonEditTag.UseVisualStyleBackColor = true;
            this.buttonEditTag.Click += new System.EventHandler(this.buttonEditTag_Click);
            // 
            // sliderVolume
            // 
            this.sliderVolume.Location = new System.Drawing.Point(257, 396);
            this.sliderVolume.Name = "sliderVolume";
            this.sliderVolume.Size = new System.Drawing.Size(320, 45);
            this.sliderVolume.TabIndex = 4;
            this.sliderVolume.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.sliderVolume.Value = 10;
            this.sliderVolume.Scroll += new System.EventHandler(this.sliderVolume_Scroll);
            // 
            // buttonNext
            // 
            this.buttonNext.BackgroundImage = ((System.Drawing.Image) (resources.GetObject("buttonNext.BackgroundImage")));
            this.buttonNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonNext.Location = new System.Drawing.Point(192, 396);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(50, 42);
            this.buttonNext.TabIndex = 2;
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonPrevious
            // 
            this.buttonPrevious.BackgroundImage = ((System.Drawing.Image) (resources.GetObject("buttonPrevious.BackgroundImage")));
            this.buttonPrevious.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonPrevious.Location = new System.Drawing.Point(12, 397);
            this.buttonPrevious.Name = "buttonPrevious";
            this.buttonPrevious.Size = new System.Drawing.Size(50, 41);
            this.buttonPrevious.TabIndex = 3;
            this.buttonPrevious.UseVisualStyleBackColor = true;
            this.buttonPrevious.Click += new System.EventHandler(this.buttonPrevious_Click);
            // 
            // buttonPlayPause
            // 
            this.buttonPlayPause.BackgroundImage = ((System.Drawing.Image) (resources.GetObject("buttonPlayPause.BackgroundImage")));
            this.buttonPlayPause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonPlayPause.Location = new System.Drawing.Point(102, 397);
            this.buttonPlayPause.Name = "buttonPlayPause";
            this.buttonPlayPause.Size = new System.Drawing.Size(50, 41);
            this.buttonPlayPause.TabIndex = 1;
            this.buttonPlayPause.UseVisualStyleBackColor = true;
            this.buttonPlayPause.Click += new System.EventHandler(this.buttonPlayPause_Click);
            // 
            // tableLayoutSongTags
            // 
            this.tableLayoutSongTags.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutSongTags.ColumnCount = 2;
            this.tableLayoutSongTags.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36F));
            this.tableLayoutSongTags.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64F));
            this.tableLayoutSongTags.Controls.Add(this.labelSongArtist, 0, 1);
            this.tableLayoutSongTags.Controls.Add(this.labelSongAlbum, 0, 2);
            this.tableLayoutSongTags.Controls.Add(this.labelSongTitle, 0, 0);
            this.tableLayoutSongTags.Controls.Add(this.labelSongYear, 0, 3);
            this.tableLayoutSongTags.Controls.Add(this.labelSongGenre, 0, 4);
            this.tableLayoutSongTags.Controls.Add(this.labelSongComposer, 0, 5);
            this.tableLayoutSongTags.Controls.Add(this.labelSongArtistValue, 1, 1);
            this.tableLayoutSongTags.Controls.Add(this.labelSongAlbumValue, 1, 2);
            this.tableLayoutSongTags.Controls.Add(this.labelSongYearValue, 1, 3);
            this.tableLayoutSongTags.Controls.Add(this.labelSongGenreValue, 1, 4);
            this.tableLayoutSongTags.Controls.Add(this.labelSongComposerValue, 1, 5);
            this.tableLayoutSongTags.Controls.Add(this.labelSongTitleValue, 1, 0);
            this.tableLayoutSongTags.Location = new System.Drawing.Point(257, 43);
            this.tableLayoutSongTags.Name = "tableLayoutSongTags";
            this.tableLayoutSongTags.RowCount = 6;
            this.tableLayoutSongTags.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutSongTags.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16F));
            this.tableLayoutSongTags.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16F));
            this.tableLayoutSongTags.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16F));
            this.tableLayoutSongTags.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16F));
            this.tableLayoutSongTags.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16F));
            this.tableLayoutSongTags.Size = new System.Drawing.Size(320, 342);
            this.tableLayoutSongTags.TabIndex = 7;
            // 
            // labelSongArtist
            // 
            this.labelSongArtist.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSongArtist.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelSongArtist.Location = new System.Drawing.Point(4, 69);
            this.labelSongArtist.Name = "labelSongArtist";
            this.labelSongArtist.Size = new System.Drawing.Size(108, 53);
            this.labelSongArtist.TabIndex = 0;
            this.labelSongArtist.Text = "Artist : ";
            this.labelSongArtist.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSongAlbum
            // 
            this.labelSongAlbum.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSongAlbum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelSongAlbum.Location = new System.Drawing.Point(4, 123);
            this.labelSongAlbum.Name = "labelSongAlbum";
            this.labelSongAlbum.Size = new System.Drawing.Size(108, 53);
            this.labelSongAlbum.TabIndex = 1;
            this.labelSongAlbum.Text = "Album : ";
            this.labelSongAlbum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSongTitle
            // 
            this.labelSongTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSongTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelSongTitle.Location = new System.Drawing.Point(4, 1);
            this.labelSongTitle.Name = "labelSongTitle";
            this.labelSongTitle.Size = new System.Drawing.Size(108, 67);
            this.labelSongTitle.TabIndex = 9;
            this.labelSongTitle.Text = "Title :";
            this.labelSongTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSongYear
            // 
            this.labelSongYear.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSongYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelSongYear.Location = new System.Drawing.Point(4, 177);
            this.labelSongYear.Name = "labelSongYear";
            this.labelSongYear.Size = new System.Drawing.Size(108, 53);
            this.labelSongYear.TabIndex = 2;
            this.labelSongYear.Text = "Year :";
            this.labelSongYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSongGenre
            // 
            this.labelSongGenre.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSongGenre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelSongGenre.Location = new System.Drawing.Point(4, 231);
            this.labelSongGenre.Name = "labelSongGenre";
            this.labelSongGenre.Size = new System.Drawing.Size(108, 53);
            this.labelSongGenre.TabIndex = 3;
            this.labelSongGenre.Text = "Genre : ";
            this.labelSongGenre.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSongComposer
            // 
            this.labelSongComposer.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSongComposer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelSongComposer.Location = new System.Drawing.Point(4, 285);
            this.labelSongComposer.Name = "labelSongComposer";
            this.labelSongComposer.Size = new System.Drawing.Size(108, 56);
            this.labelSongComposer.TabIndex = 4;
            this.labelSongComposer.Text = "Composer :";
            this.labelSongComposer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSongArtistValue
            // 
            this.labelSongArtistValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSongArtistValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelSongArtistValue.Location = new System.Drawing.Point(119, 69);
            this.labelSongArtistValue.Name = "labelSongArtistValue";
            this.labelSongArtistValue.Size = new System.Drawing.Size(197, 53);
            this.labelSongArtistValue.TabIndex = 5;
            this.labelSongArtistValue.Text = "N/A";
            this.labelSongArtistValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSongAlbumValue
            // 
            this.labelSongAlbumValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSongAlbumValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelSongAlbumValue.Location = new System.Drawing.Point(119, 123);
            this.labelSongAlbumValue.Name = "labelSongAlbumValue";
            this.labelSongAlbumValue.Size = new System.Drawing.Size(197, 53);
            this.labelSongAlbumValue.TabIndex = 6;
            this.labelSongAlbumValue.Text = "N/A";
            this.labelSongAlbumValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSongYearValue
            // 
            this.labelSongYearValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSongYearValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelSongYearValue.Location = new System.Drawing.Point(119, 177);
            this.labelSongYearValue.Name = "labelSongYearValue";
            this.labelSongYearValue.Size = new System.Drawing.Size(197, 53);
            this.labelSongYearValue.TabIndex = 7;
            this.labelSongYearValue.Text = "N/A";
            this.labelSongYearValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSongGenreValue
            // 
            this.labelSongGenreValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSongGenreValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelSongGenreValue.Location = new System.Drawing.Point(119, 231);
            this.labelSongGenreValue.Name = "labelSongGenreValue";
            this.labelSongGenreValue.Size = new System.Drawing.Size(197, 53);
            this.labelSongGenreValue.TabIndex = 8;
            this.labelSongGenreValue.Text = "N/A";
            this.labelSongGenreValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSongComposerValue
            // 
            this.labelSongComposerValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSongComposerValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelSongComposerValue.Location = new System.Drawing.Point(119, 285);
            this.labelSongComposerValue.Name = "labelSongComposerValue";
            this.labelSongComposerValue.Size = new System.Drawing.Size(197, 56);
            this.labelSongComposerValue.TabIndex = 9;
            this.labelSongComposerValue.Text = "N/A";
            this.labelSongComposerValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSongTitleValue
            // 
            this.labelSongTitleValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSongTitleValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelSongTitleValue.Location = new System.Drawing.Point(119, 1);
            this.labelSongTitleValue.Name = "labelSongTitleValue";
            this.labelSongTitleValue.Size = new System.Drawing.Size(197, 67);
            this.labelSongTitleValue.TabIndex = 10;
            this.labelSongTitleValue.Text = "N/A";
            this.labelSongTitleValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureSong
            // 
            this.pictureSong.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureSong.Location = new System.Drawing.Point(12, 43);
            this.pictureSong.Name = "pictureSong";
            this.pictureSong.Size = new System.Drawing.Size(230, 230);
            this.pictureSong.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureSong.TabIndex = 8;
            this.pictureSong.TabStop = false;
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.ShowNewFolderButton = false;
            // 
            // labelErrorMessage
            // 
            this.labelErrorMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelErrorMessage.ForeColor = System.Drawing.Color.Red;
            this.labelErrorMessage.Location = new System.Drawing.Point(12, 7);
            this.labelErrorMessage.Name = "labelErrorMessage";
            this.labelErrorMessage.Size = new System.Drawing.Size(526, 24);
            this.labelErrorMessage.TabIndex = 10;
            this.labelErrorMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68F));
            this.tableLayoutPanel1.Controls.Add(this.labelSongCodec, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelSongBitrate, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelSongFrequency, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelSongLength, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelSongCodecValue, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelSongBitrateValue, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelSongFrequencyValue, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelSongLengthValue, 1, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 290);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(230, 95);
            this.tableLayoutPanel1.TabIndex = 11;
            // 
            // labelSongCodec
            // 
            this.labelSongCodec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSongCodec.Location = new System.Drawing.Point(5, 2);
            this.labelSongCodec.Name = "labelSongCodec";
            this.labelSongCodec.Size = new System.Drawing.Size(65, 21);
            this.labelSongCodec.TabIndex = 0;
            this.labelSongCodec.Text = "Codec :";
            this.labelSongCodec.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSongBitrate
            // 
            this.labelSongBitrate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSongBitrate.Location = new System.Drawing.Point(5, 25);
            this.labelSongBitrate.Name = "labelSongBitrate";
            this.labelSongBitrate.Size = new System.Drawing.Size(65, 21);
            this.labelSongBitrate.TabIndex = 1;
            this.labelSongBitrate.Text = "Bitrate :";
            this.labelSongBitrate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSongFrequency
            // 
            this.labelSongFrequency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSongFrequency.Location = new System.Drawing.Point(5, 48);
            this.labelSongFrequency.Name = "labelSongFrequency";
            this.labelSongFrequency.Size = new System.Drawing.Size(65, 21);
            this.labelSongFrequency.TabIndex = 2;
            this.labelSongFrequency.Text = "Frequency :";
            this.labelSongFrequency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSongLength
            // 
            this.labelSongLength.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSongLength.Location = new System.Drawing.Point(5, 71);
            this.labelSongLength.Name = "labelSongLength";
            this.labelSongLength.Size = new System.Drawing.Size(65, 22);
            this.labelSongLength.TabIndex = 3;
            this.labelSongLength.Text = "Length :";
            this.labelSongLength.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSongCodecValue
            // 
            this.labelSongCodecValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSongCodecValue.Location = new System.Drawing.Point(78, 2);
            this.labelSongCodecValue.Name = "labelSongCodecValue";
            this.labelSongCodecValue.Size = new System.Drawing.Size(147, 21);
            this.labelSongCodecValue.TabIndex = 4;
            this.labelSongCodecValue.Text = "N/A";
            this.labelSongCodecValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSongBitrateValue
            // 
            this.labelSongBitrateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSongBitrateValue.Location = new System.Drawing.Point(78, 25);
            this.labelSongBitrateValue.Name = "labelSongBitrateValue";
            this.labelSongBitrateValue.Size = new System.Drawing.Size(147, 21);
            this.labelSongBitrateValue.TabIndex = 5;
            this.labelSongBitrateValue.Text = "N/A";
            this.labelSongBitrateValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSongFrequencyValue
            // 
            this.labelSongFrequencyValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSongFrequencyValue.Location = new System.Drawing.Point(78, 48);
            this.labelSongFrequencyValue.Name = "labelSongFrequencyValue";
            this.labelSongFrequencyValue.Size = new System.Drawing.Size(147, 21);
            this.labelSongFrequencyValue.TabIndex = 6;
            this.labelSongFrequencyValue.Text = "N/A";
            this.labelSongFrequencyValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSongLengthValue
            // 
            this.labelSongLengthValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSongLengthValue.Location = new System.Drawing.Point(78, 71);
            this.labelSongLengthValue.Name = "labelSongLengthValue";
            this.labelSongLengthValue.Size = new System.Drawing.Size(147, 22);
            this.labelSongLengthValue.TabIndex = 7;
            this.labelSongLengthValue.Text = "N/A";
            this.labelSongLengthValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonExport
            // 
            this.buttonExport.Enabled = false;
            this.buttonExport.Location = new System.Drawing.Point(583, 395);
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new System.Drawing.Size(205, 30);
            this.buttonExport.TabIndex = 12;
            this.buttonExport.Text = "Export...";
            this.buttonExport.UseVisualStyleBackColor = true;
            this.buttonExport.Click += new System.EventHandler(this.buttonExportAsync_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(583, 431);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(205, 10);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 13;
            this.progressBar1.Visible = false;
            // 
            // Primary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.buttonExport);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.labelErrorMessage);
            this.Controls.Add(this.pictureSong);
            this.Controls.Add(this.tableLayoutSongTags);
            this.Controls.Add(this.buttonPlayPause);
            this.Controls.Add(this.buttonPrevious);
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.sliderVolume);
            this.Controls.Add(this.buttonEditTag);
            this.Controls.Add(this.buttonLoadDirectory);
            this.Controls.Add(this.listLoadedSongs);
            this.Name = "Primary";
            this.Text = "Music Player";
            ((System.ComponentModel.ISupportInitialize) (this.sliderVolume)).EndInit();
            this.tableLayoutSongTags.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.pictureSong)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog2;

        private System.Windows.Forms.ProgressBar progressBar1;

        private System.Windows.Forms.Button buttonExport;

        private System.Windows.Forms.Label labelSongTitleValue;

        private System.Windows.Forms.Label labelSongCodec;
        private System.Windows.Forms.Label labelSongBitrate;
        private System.Windows.Forms.Label labelSongFrequency;
        private System.Windows.Forms.Label labelSongLength;
        private System.Windows.Forms.Label labelSongCodecValue;
        private System.Windows.Forms.Label labelSongBitrateValue;
        private System.Windows.Forms.Label labelSongFrequencyValue;
        private System.Windows.Forms.Label labelSongLengthValue;

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

        private System.Windows.Forms.Label labelSongArtistValue;
        private System.Windows.Forms.Label labelSongAlbumValue;
        private System.Windows.Forms.Label labelSongYearValue;
        private System.Windows.Forms.Label labelSongGenreValue;
        private System.Windows.Forms.Label labelSongComposerValue;

        private System.Windows.Forms.Label labelErrorMessage;

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;

        private System.Windows.Forms.Label labelSongArtist;
        private System.Windows.Forms.Label labelSongAlbum;
        private System.Windows.Forms.Label labelSongYear;
        private System.Windows.Forms.Label labelSongGenre;
        private System.Windows.Forms.Label labelSongComposer;

        private System.Windows.Forms.Label labelSongTitle;
        private System.Windows.Forms.PictureBox pictureSong;

        private System.Windows.Forms.TableLayoutPanel tableLayoutSongTags;

        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrevious;
        private System.Windows.Forms.Button buttonPlayPause;

        private System.Windows.Forms.Button buttonLoadDirectory;
        private System.Windows.Forms.Button buttonEditTag;
        private System.Windows.Forms.ListBox listLoadedSongs;
        private System.Windows.Forms.TrackBar sliderVolume;

        #endregion
    }
}

