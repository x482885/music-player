﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Music_Player.Forms
{
    partial class TagEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonChangesApply = new System.Windows.Forms.Button();
            this.buttonChangesReset = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanelValues = new System.Windows.Forms.TableLayoutPanel();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelArtists = new System.Windows.Forms.Label();
            this.labelAlbum = new System.Windows.Forms.Label();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelTrack = new System.Windows.Forms.Label();
            this.labelGenre = new System.Windows.Forms.Label();
            this.labelComment = new System.Windows.Forms.Label();
            this.labelAlbumArtists = new System.Windows.Forms.Label();
            this.labelComposers = new System.Windows.Forms.Label();
            this.labelDiscNumber = new System.Windows.Forms.Label();
            this.textBoxDiscNumber = new System.Windows.Forms.TextBox();
            this.textBoxComposers = new System.Windows.Forms.TextBox();
            this.textBoxAlbumArtists = new System.Windows.Forms.TextBox();
            this.textBoxComment = new System.Windows.Forms.TextBox();
            this.textBoxGenres = new System.Windows.Forms.TextBox();
            this.textBoxTrack = new System.Windows.Forms.TextBox();
            this.textBoxYear = new System.Windows.Forms.TextBox();
            this.textBoxAlbum = new System.Windows.Forms.TextBox();
            this.textBoxArtists = new System.Windows.Forms.TextBox();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.labelCover = new System.Windows.Forms.Label();
            this.tableLayoutPanelImageControls = new System.Windows.Forms.TableLayoutPanel();
            this.buttonAddCover = new System.Windows.Forms.Button();
            this.buttonRemoveCover = new System.Windows.Forms.Button();
            this.labelCoverStatus = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.labelError = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.buttonOK = new System.Windows.Forms.Button();
            this.tableLayoutPanelControls = new System.Windows.Forms.TableLayoutPanel();
            this.panel1.SuspendLayout();
            this.tableLayoutPanelValues.SuspendLayout();
            this.tableLayoutPanelImageControls.SuspendLayout();
            this.tableLayoutPanelControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonChangesApply
            // 
            this.buttonChangesApply.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonChangesApply.Location = new System.Drawing.Point(192, 3);
            this.buttonChangesApply.Name = "buttonChangesApply";
            this.buttonChangesApply.Size = new System.Drawing.Size(78, 36);
            this.buttonChangesApply.TabIndex = 1;
            this.buttonChangesApply.Text = "Apply";
            this.buttonChangesApply.UseVisualStyleBackColor = true;
            this.buttonChangesApply.Click += new System.EventHandler(this.buttonChangesApply_Click);
            // 
            // buttonChangesReset
            // 
            this.buttonChangesReset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonChangesReset.Location = new System.Drawing.Point(3, 3);
            this.buttonChangesReset.Name = "buttonChangesReset";
            this.buttonChangesReset.Size = new System.Drawing.Size(78, 36);
            this.buttonChangesReset.TabIndex = 2;
            this.buttonChangesReset.Text = "Reset";
            this.buttonChangesReset.UseVisualStyleBackColor = true;
            this.buttonChangesReset.Click += new System.EventHandler(this.buttonChangesReset_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.tableLayoutPanelValues);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(381, 343);
            this.panel1.TabIndex = 3;
            // 
            // tableLayoutPanelValues
            // 
            this.tableLayoutPanelValues.AutoSize = true;
            this.tableLayoutPanelValues.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanelValues.ColumnCount = 2;
            this.tableLayoutPanelValues.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanelValues.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelValues.Controls.Add(this.labelTitle, 0, 0);
            this.tableLayoutPanelValues.Controls.Add(this.labelArtists, 0, 1);
            this.tableLayoutPanelValues.Controls.Add(this.labelAlbum, 0, 2);
            this.tableLayoutPanelValues.Controls.Add(this.labelYear, 0, 3);
            this.tableLayoutPanelValues.Controls.Add(this.labelTrack, 0, 4);
            this.tableLayoutPanelValues.Controls.Add(this.labelGenre, 0, 5);
            this.tableLayoutPanelValues.Controls.Add(this.labelComment, 0, 6);
            this.tableLayoutPanelValues.Controls.Add(this.labelAlbumArtists, 0, 7);
            this.tableLayoutPanelValues.Controls.Add(this.labelComposers, 0, 8);
            this.tableLayoutPanelValues.Controls.Add(this.labelDiscNumber, 0, 9);
            this.tableLayoutPanelValues.Controls.Add(this.textBoxDiscNumber, 1, 9);
            this.tableLayoutPanelValues.Controls.Add(this.textBoxComposers, 1, 8);
            this.tableLayoutPanelValues.Controls.Add(this.textBoxAlbumArtists, 1, 7);
            this.tableLayoutPanelValues.Controls.Add(this.textBoxComment, 1, 6);
            this.tableLayoutPanelValues.Controls.Add(this.textBoxGenres, 1, 5);
            this.tableLayoutPanelValues.Controls.Add(this.textBoxTrack, 1, 4);
            this.tableLayoutPanelValues.Controls.Add(this.textBoxYear, 1, 3);
            this.tableLayoutPanelValues.Controls.Add(this.textBoxAlbum, 1, 2);
            this.tableLayoutPanelValues.Controls.Add(this.textBoxArtists, 1, 1);
            this.tableLayoutPanelValues.Controls.Add(this.textBoxTitle, 1, 0);
            this.tableLayoutPanelValues.Controls.Add(this.labelCover, 0, 10);
            this.tableLayoutPanelValues.Controls.Add(this.tableLayoutPanelImageControls, 1, 10);
            this.tableLayoutPanelValues.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanelValues.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelValues.Name = "tableLayoutPanelValues";
            this.tableLayoutPanelValues.RowCount = 11;
            this.tableLayoutPanelValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanelValues.Size = new System.Drawing.Size(381, 340);
            this.tableLayoutPanelValues.TabIndex = 0;
            // 
            // labelTitle
            // 
            this.labelTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelTitle.Location = new System.Drawing.Point(3, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(104, 30);
            this.labelTitle.TabIndex = 2;
            this.labelTitle.Text = "Title";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelArtists
            // 
            this.labelArtists.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelArtists.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelArtists.Location = new System.Drawing.Point(3, 30);
            this.labelArtists.Name = "labelArtists";
            this.labelArtists.Size = new System.Drawing.Size(104, 30);
            this.labelArtists.TabIndex = 3;
            this.labelArtists.Text = "Artists";
            this.labelArtists.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAlbum
            // 
            this.labelAlbum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAlbum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelAlbum.Location = new System.Drawing.Point(3, 60);
            this.labelAlbum.Name = "labelAlbum";
            this.labelAlbum.Size = new System.Drawing.Size(104, 30);
            this.labelAlbum.TabIndex = 4;
            this.labelAlbum.Text = "Album";
            this.labelAlbum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelYear
            // 
            this.labelYear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelYear.Location = new System.Drawing.Point(3, 90);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(104, 30);
            this.labelYear.TabIndex = 5;
            this.labelYear.Text = "Year";
            this.labelYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTrack
            // 
            this.labelTrack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTrack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelTrack.Location = new System.Drawing.Point(3, 120);
            this.labelTrack.Name = "labelTrack";
            this.labelTrack.Size = new System.Drawing.Size(104, 30);
            this.labelTrack.TabIndex = 6;
            this.labelTrack.Text = "Track";
            this.labelTrack.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelGenre
            // 
            this.labelGenre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelGenre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelGenre.Location = new System.Drawing.Point(3, 150);
            this.labelGenre.Name = "labelGenre";
            this.labelGenre.Size = new System.Drawing.Size(104, 30);
            this.labelGenre.TabIndex = 7;
            this.labelGenre.Text = "Genres";
            this.labelGenre.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelComment
            // 
            this.labelComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelComment.Location = new System.Drawing.Point(3, 180);
            this.labelComment.Name = "labelComment";
            this.labelComment.Size = new System.Drawing.Size(104, 30);
            this.labelComment.TabIndex = 8;
            this.labelComment.Text = "Comment";
            this.labelComment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAlbumArtists
            // 
            this.labelAlbumArtists.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAlbumArtists.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelAlbumArtists.Location = new System.Drawing.Point(3, 210);
            this.labelAlbumArtists.Name = "labelAlbumArtists";
            this.labelAlbumArtists.Size = new System.Drawing.Size(104, 30);
            this.labelAlbumArtists.TabIndex = 9;
            this.labelAlbumArtists.Text = "Album Artists";
            this.labelAlbumArtists.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelComposers
            // 
            this.labelComposers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelComposers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelComposers.Location = new System.Drawing.Point(3, 240);
            this.labelComposers.Name = "labelComposers";
            this.labelComposers.Size = new System.Drawing.Size(104, 30);
            this.labelComposers.TabIndex = 10;
            this.labelComposers.Text = "Composers";
            this.labelComposers.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDiscNumber
            // 
            this.labelDiscNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelDiscNumber.Location = new System.Drawing.Point(3, 270);
            this.labelDiscNumber.Name = "labelDiscNumber";
            this.labelDiscNumber.Size = new System.Drawing.Size(104, 30);
            this.labelDiscNumber.TabIndex = 11;
            this.labelDiscNumber.Text = "Disc";
            this.labelDiscNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxDiscNumber
            // 
            this.textBoxDiscNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxDiscNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.textBoxDiscNumber.Location = new System.Drawing.Point(113, 273);
            this.textBoxDiscNumber.Name = "textBoxDiscNumber";
            this.textBoxDiscNumber.Size = new System.Drawing.Size(265, 24);
            this.textBoxDiscNumber.TabIndex = 14;
            // 
            // textBoxComposers
            // 
            this.textBoxComposers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxComposers.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.textBoxComposers.Location = new System.Drawing.Point(113, 243);
            this.textBoxComposers.Name = "textBoxComposers";
            this.textBoxComposers.Size = new System.Drawing.Size(265, 24);
            this.textBoxComposers.TabIndex = 15;
            this.textBoxComposers.MouseHover += (_,_) => this.toolTip1.SetToolTip(textBoxComposers,"Enter the names of the composers separated by comma \',\' .\r\nFirst composer is the main somposer.\r\n\r\nExample: \"Brian John" + "son, Angus Young, Malcolm Young\"");
            // 
            // textBoxAlbumArtists
            // 
            this.textBoxAlbumArtists.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxAlbumArtists.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.textBoxAlbumArtists.Location = new System.Drawing.Point(113, 213);
            this.textBoxAlbumArtists.Name = "textBoxAlbumArtists";
            this.textBoxAlbumArtists.Size = new System.Drawing.Size(265, 24);
            this.textBoxAlbumArtists.TabIndex = 16;
            this.textBoxAlbumArtists.MouseHover += (_,_) => this.toolTip1.SetToolTip(textBoxAlbumArtists,"Enter the names of the artists separated by comma \',\' .\r\nFirst artist is the main artist.\r\nArtists that contributed" + " to the creation of the album.\r\n\r\nExample: \"Angus Young, Brian Johnson, Phil Rud" + "d\"");
            // 
            // textBoxComment
            // 
            this.textBoxComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.textBoxComment.Location = new System.Drawing.Point(113, 183);
            this.textBoxComment.Name = "textBoxComment";
            this.textBoxComment.Size = new System.Drawing.Size(265, 24);
            this.textBoxComment.TabIndex = 17;
            // 
            // textBoxGenres
            // 
            this.textBoxGenres.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxGenres.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.textBoxGenres.Location = new System.Drawing.Point(113, 153);
            this.textBoxGenres.Name = "textBoxGenres";
            this.textBoxGenres.Size = new System.Drawing.Size(265, 24);
            this.textBoxGenres.TabIndex = 18;
            this.textBoxGenres.MouseHover += (_,_) => this.toolTip1.SetToolTip(this.textBoxGenres,"Enter the names of the genres separated by comma \',\' .\r\nFirst genre is the main genre.\r\n\r\nExample: \"Rock, Blues, " + "Instrumental\"");
            // 
            // textBoxTrack
            // 
            this.textBoxTrack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxTrack.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.textBoxTrack.Location = new System.Drawing.Point(113, 123);
            this.textBoxTrack.Name = "textBoxTrack";
            this.textBoxTrack.Size = new System.Drawing.Size(265, 24);
            this.textBoxTrack.TabIndex = 19;
            // 
            // textBoxYear
            // 
            this.textBoxYear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.textBoxYear.Location = new System.Drawing.Point(113, 93);
            this.textBoxYear.Name = "textBoxYear";
            this.textBoxYear.Size = new System.Drawing.Size(265, 24);
            this.textBoxYear.TabIndex = 20;
            // 
            // textBoxAlbum
            // 
            this.textBoxAlbum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxAlbum.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.textBoxAlbum.Location = new System.Drawing.Point(113, 63);
            this.textBoxAlbum.Name = "textBoxAlbum";
            this.textBoxAlbum.Size = new System.Drawing.Size(265, 24);
            this.textBoxAlbum.TabIndex = 21;
            // 
            // textBoxArtists
            // 
            this.textBoxArtists.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxArtists.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.textBoxArtists.Location = new System.Drawing.Point(113, 33);
            this.textBoxArtists.Name = "textBoxArtists";
            this.textBoxArtists.Size = new System.Drawing.Size(265, 24);
            this.textBoxArtists.TabIndex = 22;
            this.textBoxArtists.MouseHover += (_,_) => this.toolTip1.SetToolTip(this.textBoxArtists,"Enter the names of the artists separated by comma \',\' .\r\nFirst artist is the main artist.\r\n\r\nExample: \"Angus Young," + " Brian Johnson, Phil Rudd\"");
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.textBoxTitle.Location = new System.Drawing.Point(113, 3);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(265, 24);
            this.textBoxTitle.TabIndex = 23;
            // 
            // labelCover
            // 
            this.labelCover.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCover.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelCover.Location = new System.Drawing.Point(3, 300);
            this.labelCover.Name = "labelCover";
            this.labelCover.Size = new System.Drawing.Size(104, 40);
            this.labelCover.TabIndex = 24;
            this.labelCover.Text = "Cover";
            this.labelCover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanelImageControls
            // 
            this.tableLayoutPanelImageControls.ColumnCount = 3;
            this.tableLayoutPanelImageControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tableLayoutPanelImageControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tableLayoutPanelImageControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelImageControls.Controls.Add(this.buttonAddCover, 1, 0);
            this.tableLayoutPanelImageControls.Controls.Add(this.buttonRemoveCover, 0, 0);
            this.tableLayoutPanelImageControls.Controls.Add(this.labelCoverStatus, 2, 0);
            this.tableLayoutPanelImageControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelImageControls.Location = new System.Drawing.Point(113, 303);
            this.tableLayoutPanelImageControls.Name = "tableLayoutPanelImageControls";
            this.tableLayoutPanelImageControls.RowCount = 1;
            this.tableLayoutPanelImageControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelImageControls.Size = new System.Drawing.Size(265, 34);
            this.tableLayoutPanelImageControls.TabIndex = 25;
            // 
            // buttonAddCover
            // 
            this.buttonAddCover.Location = new System.Drawing.Point(77, 3);
            this.buttonAddCover.Name = "buttonAddCover";
            this.buttonAddCover.Size = new System.Drawing.Size(68, 28);
            this.buttonAddCover.TabIndex = 0;
            this.buttonAddCover.Text = "Add...";
            this.buttonAddCover.UseVisualStyleBackColor = true;
            this.buttonAddCover.Click += new System.EventHandler(this.buttonAddCover_Click);
            // 
            // buttonRemoveCover
            // 
            this.buttonRemoveCover.Location = new System.Drawing.Point(3, 3);
            this.buttonRemoveCover.Name = "buttonRemoveCover";
            this.buttonRemoveCover.Size = new System.Drawing.Size(68, 28);
            this.buttonRemoveCover.TabIndex = 1;
            this.buttonRemoveCover.Text = "Remove";
            this.buttonRemoveCover.UseVisualStyleBackColor = true;
            this.buttonRemoveCover.Click += new System.EventHandler(this.buttonRemoveCover_Click);
            // 
            // labelCoverStatus
            // 
            this.labelCoverStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCoverStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelCoverStatus.Location = new System.Drawing.Point(151, 0);
            this.labelCoverStatus.Name = "labelCoverStatus";
            this.labelCoverStatus.Size = new System.Drawing.Size(111, 34);
            this.labelCoverStatus.TabIndex = 2;
            this.labelCoverStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif;" + " *.png";
            // 
            // labelError
            // 
            this.labelError.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.labelError.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labelError.ForeColor = System.Drawing.Color.Red;
            this.labelError.Location = new System.Drawing.Point(12, 346);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(357, 20);
            this.labelError.TabIndex = 4;
            this.labelError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonOK
            // 
            this.buttonOK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonOK.Location = new System.Drawing.Point(276, 3);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(78, 36);
            this.buttonOK.TabIndex = 5;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // tableLayoutPanelControls
            // 
            this.tableLayoutPanelControls.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelControls.ColumnCount = 4;
            this.tableLayoutPanelControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this.tableLayoutPanelControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this.tableLayoutPanelControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this.tableLayoutPanelControls.Controls.Add(this.buttonChangesReset, 0, 0);
            this.tableLayoutPanelControls.Controls.Add(this.buttonChangesApply, 2, 0);
            this.tableLayoutPanelControls.Controls.Add(this.buttonOK, 3, 0);
            this.tableLayoutPanelControls.Location = new System.Drawing.Point(12, 369);
            this.tableLayoutPanelControls.Name = "tableLayoutPanelControls";
            this.tableLayoutPanelControls.RowCount = 1;
            this.tableLayoutPanelControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanelControls.Size = new System.Drawing.Size(357, 42);
            this.tableLayoutPanelControls.TabIndex = 6;
            // 
            // TagEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 423);
            this.Controls.Add(this.tableLayoutPanelControls);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.panel1);
            this.Name = "TagEditor";
            this.Text = "Tag Editor";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanelValues.ResumeLayout(false);
            this.tableLayoutPanelValues.PerformLayout();
            this.tableLayoutPanelImageControls.ResumeLayout(false);
            this.tableLayoutPanelControls.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelControls;

        private System.Windows.Forms.Button buttonOK;

        private System.Windows.Forms.Label labelCoverStatus;

        private System.Windows.Forms.Button buttonRemoveCover;

        private System.Windows.Forms.Button buttonAddCover;

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelImageControls;

        private System.Windows.Forms.ToolTip toolTip1;

        private System.Windows.Forms.Label labelError;

        private System.Windows.Forms.OpenFileDialog openFileDialog1;

        private System.Windows.Forms.Label labelCover;

        private System.Windows.Forms.TextBox textBoxArtists;
        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.TextBox textBoxDiscNumber;
        private System.Windows.Forms.TextBox textBoxComposers;
        private System.Windows.Forms.TextBox textBoxAlbumArtists;
        private System.Windows.Forms.TextBox textBoxComment;
        private System.Windows.Forms.TextBox textBoxGenres;
        private System.Windows.Forms.TextBox textBoxTrack;
        private System.Windows.Forms.TextBox textBoxYear;
        private System.Windows.Forms.TextBox textBoxAlbum;

        private System.Windows.Forms.Label labelDiscNumber;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Label labelTrack;
        private System.Windows.Forms.Label labelGenre;
        private System.Windows.Forms.Label labelComment;
        private System.Windows.Forms.Label labelAlbumArtists;
        private System.Windows.Forms.Label labelComposers;

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelArtists;
        private System.Windows.Forms.Label labelAlbum;

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelValues;

        private System.Windows.Forms.Panel panel1;

        private System.Windows.Forms.Button buttonChangesApply;
        private System.Windows.Forms.Button buttonChangesReset;
        
        #endregion
    }
}