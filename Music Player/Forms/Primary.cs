﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Music_Player.InputOutput;
using Music_Player.Models;
using NAudio.Wave;
using TagLib;

namespace Music_Player.Forms
{
    public partial class Primary : Form
    {
        private static readonly Image DefaultImage = Image.FromFile(Path.Combine(
            Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName, @"Assets",
            "defaultAlbumImage.png"));

        private static readonly Image PlayIcon = Image.FromFile(Path.Combine(
            Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName, @"Assets", "play.png"));

        private static readonly Image PauseIcon = Image.FromFile(Path.Combine(
            Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName, @"Assets", "pause.png"));

        /// <summary>
        ///     Instance of the player
        /// </summary>
        private static readonly WaveOutEvent OutputDevice = new();

        /// <summary>
        ///     Currently playing audio file
        /// </summary>
        private AudioFileReader _currentReader;

        /// <summary>
        ///     Currently selected/initialized song
        /// </summary>
        private Song _currentSong;

        /// <summary>
        ///     List of currently loaded songs
        /// </summary>
        private List<Song> _loadedSongs = new();

        public Primary()
        {
            InitializeComponent();
            pictureSong.Image = DefaultImage;
            OutputDevice.PlaybackStopped += (_, _) =>
            {
                buttonPlayPause.BackgroundImage = PlayIcon;
            } ;
            OutputDevice.Volume = 1f;
        }

        /// <summary>
        ///     Method initializing player with previous song
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPrevious_Click(object sender, EventArgs e)
        {
            PlayerInit(-1, 0);
        }

        /// <summary>
        ///     Method initializing player with next song
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNext_Click(object sender, EventArgs e)
        {
            PlayerInit(1, _loadedSongs.Count - 1);
        }

        /// <summary>
        ///     Method responsible for switching between the possible functionality of play/pause button
        ///     Sends the music play method of player to thread pool in case player is paused or just initialized
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPlayPause_Click(object sender, EventArgs e)
        {
            if (_currentSong == null) return;

            switch (OutputDevice.PlaybackState)
            {
                case PlaybackState.Playing:
                    buttonPlayPause.BackgroundImage = PlayIcon;
                    OutputDevice.Pause();
                    break;
                case PlaybackState.Paused:
                    buttonPlayPause.BackgroundImage = PauseIcon;
                    ThreadPool.QueueUserWorkItem(_ => OutputDevice.Play());
                    break;
                default:
                    buttonPlayPause.BackgroundImage = PauseIcon;
                    ThreadPool.QueueUserWorkItem(_ => OutputDevice.Play());
                    break;
            }
        }

        /// <summary>
        ///     Method responsible for correct parsing of loaded songs
        ///     Opens folder dialog window to select directory from which to load songs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void buttonLoadDirectoryAsync_Click(object sender, EventArgs e)
        {
            buttonLoadDirectory.Enabled = false;
            buttonEditTag.Enabled = false;
            buttonExport.Enabled = false;
            
            var tempPath = folderBrowserDialog1.SelectedPath;

            folderBrowserDialog1.ShowDialog();

            var folderPath = folderBrowserDialog1.SelectedPath;

            if (string.IsNullOrWhiteSpace(folderPath) || tempPath == folderPath)
            {
                if (listLoadedSongs.Items.Count > 0) buttonExport.Enabled = true;
                buttonLoadDirectory.Enabled = true; 
                return;
            }

            var loaded = await Task.Run(() => Loader.LoadSong(folderPath));

            if (!loaded.Any())
            {
                labelErrorMessage.Text = "Directory does not contain any MP3 files!";
                buttonLoadDirectory.Enabled = true; 
                return;
            }

            labelErrorMessage.Text = "";

            SongDispose();

            _loadedSongs = loaded.ToList();

            OutputDevice.Stop();

            _currentSong = null;

            TagLabelsManager();
            skipButtonManager();
            
            listLoadedSongs.Items.Clear();
            listLoadedSongs.Items.AddRange(loaded.Select(song => song.Name).ToArray());

            buttonExport.Enabled = true;
            buttonLoadDirectory.Enabled = true; 
        }

        /// <summary>
        ///     Opens up the tag editor form and disables this form for the duration of editing
        ///     Returns functionality to this form once the tag editor form is closed and updates displayed tag values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEditTag_Click(object sender, EventArgs e)
        {
            if (_currentSong == null)
            {
                labelErrorMessage.Text = "No song selected!";
                return;
            }

            labelErrorMessage.Text = "";
            Enabled = false;
            _currentReader?.Dispose();
            var editor = new TagEditor(_currentSong);
            editor.Show();
            editor.Closed += (_, _) =>
            {
                TagLabelsManager();
                PlayerInit(0, -1);
                Enabled = true;
            };
        }

        /// <summary>
        ///     Loads selected song into the player
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listLoadedSongs_DoubleClick(object sender, EventArgs e)
        {
            if (listLoadedSongs.SelectedItem != null)
            {
                labelErrorMessage.Text = "";
                _currentSong = _loadedSongs[listLoadedSongs.SelectedIndex];
                buttonEditTag.Enabled = true;
            }

            PlayerInit(0, -1);
        }

        /// <summary>
        ///     Method initializing the player with the appropriate audio file
        /// </summary>
        /// <param name="change"></param>
        /// <param name="margin"></param>
        private void PlayerInit(int change, int margin)
        {
            var index = _loadedSongs.IndexOf(_currentSong);

            if (index == margin || index < 0) return;

            _currentSong = _loadedSongs[index + change];
            skipButtonManager();
            TagLabelsManager();
            _currentReader?.Dispose();
            
            if (OutputDevice.PlaybackState != PlaybackState.Stopped) OutputDevice.Stop();
            
            var audioFile = new AudioFileReader($"{_loadedSongs[index + change].Path}");
            
            Console.WriteLine(_currentSong.Name);
            buttonPlayPause.BackgroundImage = PlayIcon;
            OutputDevice.Init(audioFile);
            _currentReader = audioFile;
            if (listLoadedSongs.SelectedIndex != _loadedSongs.IndexOf(_currentSong)) listLoadedSongs.SelectedIndex = _loadedSongs.IndexOf(_currentSong);
        }

        /// <summary>
        ///     Method adjusting the volume of the player
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sliderVolume_Scroll(object sender, EventArgs e)
        {
            OutputDevice.Volume = sliderVolume.Value / 10.0f;
        }

        /// <summary>
        ///     This method manages disabling of the skip buttons
        /// </summary>
        private void skipButtonManager()
        {
            if (_currentSong == _loadedSongs[0] && _currentSong == _loadedSongs[^1])
            {
                buttonNext.Enabled = false;
                buttonPrevious.Enabled = false;
            }
            else if (_currentSong == _loadedSongs[0])
            {
                buttonNext.Enabled = true;
                buttonPrevious.Enabled = false;
            }
            else if (_currentSong == _loadedSongs[^1])
            {
                buttonNext.Enabled = false;
                buttonPrevious.Enabled = true;
            }
            else
            {
                buttonNext.Enabled = true;
                buttonPrevious.Enabled = true;
            }
        }

        /// <summary>
        ///     This method reloads all of the labels about song with newest information
        ///     If no song is loaded or value of the exact tag in song is empty, sets to "N/A"
        /// </summary>
        private void TagLabelsManager()
        {
            var ms = _currentSong == null || _currentSong.Tag.Pictures.Length < 1
                ? DefaultImage
                : _currentSong.Tag.Pictures.Any(picure => picure.Type == PictureType.FrontCover)
                    ? (Bitmap) new ImageConverter().ConvertFrom(_currentSong.Tag.Pictures
                        .First(picture => picture.Type == PictureType.FrontCover).Data.Data)
                    : (Bitmap) new ImageConverter().ConvertFrom(_currentSong.Tag.Pictures[0].Data.Data);
            
            pictureSong.Image = ms;
            
            labelSongCodecValue.Text =
                _currentSong == null ? "N/A" : _currentSong.TagFile.Properties.Codecs.First().Description;
            
            labelSongBitrateValue.Text =
                _currentSong == null ? "N/A" : _currentSong.TagFile.Properties.AudioBitrate + " kbit/s";
            
            labelSongFrequencyValue.Text =
                _currentSong == null ? "N/A" : _currentSong.TagFile.Properties.AudioSampleRate + " Hz";
            
            labelSongLengthValue.Text = _currentSong == null
                ? "N/A"
                : _currentSong.TagFile.Properties.Duration.ToString("mm':'ss");
            
            labelSongTitleValue.Text = _currentSong == null || string.IsNullOrWhiteSpace(_currentSong.Tag.Title)
                ? "N/A"
                : _currentSong.Tag.Title;
            
            labelSongArtistValue.Text =
                _currentSong == null || string.IsNullOrWhiteSpace(_currentSong.Tag.FirstAlbumArtist)
                    ? "N/A"
                    : _currentSong.Tag.FirstAlbumArtist;
            
            labelSongAlbumValue.Text = _currentSong == null || string.IsNullOrWhiteSpace(_currentSong.Tag.Album)
                ? "N/A"
                : _currentSong.Tag.Album;
            
            labelSongYearValue.Text = _currentSong == null || string.IsNullOrWhiteSpace($@"{_currentSong.Tag.Year}")
                ? "N/A"
                : $@"{_currentSong.Tag.Year}";
            
            labelSongGenreValue.Text = _currentSong == null || string.IsNullOrWhiteSpace(_currentSong.Tag.FirstGenre)
                ? "N/A"
                : _currentSong.Tag.FirstGenre;
            
            labelSongComposerValue.Text =
                _currentSong == null || string.IsNullOrWhiteSpace(_currentSong.Tag.FirstComposer)
                    ? "N/A"
                    : _currentSong.Tag.FirstComposer;
        }

        /// <summary>
        ///     Method responsible for disposing of no longer needed songs
        /// </summary>
        private void SongDispose()
        {
            _currentSong?.Dispose();
            _currentReader?.Dispose();

            if (!_loadedSongs.Any()) return;
            foreach (var song in _loadedSongs) song.Dispose();
        }

        /// <summary>
        ///     Async method responsible for the export procedure
        ///     Starts Json serialization and prevents user from running another export procedure
        ///     Opens folder dialog window to select directory to which to export json files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void buttonExportAsync_Click(object sender, EventArgs e)
        {
            folderBrowserDialog2.ShowDialog();
            var path = folderBrowserDialog2.SelectedPath;
            if (string.IsNullOrWhiteSpace(path)) return;

            progressBar1.Visible = true;
            buttonExport.Enabled = false;
            buttonLoadDirectory.Enabled = false;
            buttonEditTag.Enabled = false;

            await Task.Run(() => JsonSerialize.Serialize(path, _loadedSongs));
            
            buttonExport.Enabled = true;
            buttonLoadDirectory.Enabled = true;
            if (listLoadedSongs.SelectedItem != null) buttonEditTag.Enabled = true;
            progressBar1.Visible = false;

            labelErrorMessage.ForeColor = Color.Green;
            labelErrorMessage.Text = "Succesfully exported.";
        }
        
    }
}