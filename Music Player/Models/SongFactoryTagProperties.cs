﻿using TagLib;

namespace Music_Player.Models
{
    /// <summary>
    ///     Class responsible for holding the original Tag values of the song.
    /// </summary>
    public class SongFactoryTagProperties
    {
        public SongFactoryTagProperties(Tag tag)
        {
            Title = tag.Title;
            Performers = tag.Performers;
            Album = tag.Album;
            Year = tag.Year;
            Track = tag.Track;
            Genres = tag.Genres;
            Comment = tag.Comment;
            AlbumArtists = tag.AlbumArtists;
            Composers = tag.Composers;
            Disc = tag.Disc;
            Covers = tag.Pictures;
        }

        public string Title { get; }

        public string[] Performers { get; }

        public string Album { get; }

        public uint Year { get; }

        public uint Track { get; }

        public string[] Genres { get; }

        public string Comment { get; }

        public string[] AlbumArtists { get; }

        public string[] Composers { get; }

        public uint Disc { get; }

        public IPicture[] Covers { get; }
    }
}