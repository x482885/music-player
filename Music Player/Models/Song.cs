﻿using System;
using System.Linq;
using System.Text.Json.Serialization;
using TagLib;

namespace Music_Player.Models
{
    /// <summary>
    ///     Class representing Song object with multiple properties for better accessibility
    /// </summary>
    public class Song : IDisposable
    {
        public Song(string path)
        {
            Path = path;
            TagFile = File.Create(Path);
            Tag = TagFile.Tag;
            FactoryTagProperties = new SongFactoryTagProperties(TagFile.Tag);
        }

        public string Path { get; }

        public string Name => Path.Split(@"\").Last();

        [JsonIgnore] public File TagFile { get; }

        public Tag Tag { get; }

        public SongFactoryTagProperties FactoryTagProperties { get; }

        /// <summary>
        ///     Custom dispose method, disposing the TagFile of the song
        /// </summary>
        public void Dispose()
        {
            TagFile?.Dispose();
        }
    }
}