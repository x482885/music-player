﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Music_Player.Models;
using TagLib;
using TagLib.Id3v2;

namespace Music_Player.InputOutput
{
    /// <summary>
    ///     Class responsible for loading of .mp3 and "picture format" files from directory and creating appropriate picture
    ///     format
    /// </summary>
    public static class Loader
    {
        public static IEnumerable<Song> LoadSong(string path)
        {
            var files = Directory.GetFiles(path).Where(file => file.EndsWith(".mp3"));

            var loadedSongs = files.Select(file => new Song(file)).ToList();

            if (loadedSongs.All(song => song.TagFile.Writeable && !song.TagFile.PossiblyCorrupt)) return loadedSongs;
            
            foreach (var song in loadedSongs) song.Dispose();
            return new List<Song>();
        }

        public static IPicture LoadPicture(string path)
        {
            var picture = new Picture(path);
            var frame = new AttachmentFrame(picture) {Type = PictureType.FrontCover};

            return frame;
        }
    }
}