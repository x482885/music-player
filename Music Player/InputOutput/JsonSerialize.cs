﻿using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Music_Player.Models;

namespace Music_Player.InputOutput
{
    /// <summary>
    ///     Static class for JSON serialization of songs and writing to files
    /// </summary>
    public static class JsonSerialize
    {
        public static void Serialize(string path, List<Song> listSongs)
        {
            Directory.CreateDirectory(path);

            var serializeOptions = new JsonSerializerOptions
            {
                NumberHandling = JsonNumberHandling.AllowNamedFloatingPointLiterals
            };

            Parallel.ForEach(listSongs, song =>
            {
                File.WriteAllText(Path.Combine(path, song.Name.Substring(0, song.Name.Length - 4) + ".json"),
                    JsonSerializer.Serialize(song, serializeOptions));
            });
        }
    }
}